import matplotlib.pyplot as plt
from scipy.fftpack import fft, fftfreq, fftshift
from matplotlib.pyplot import cm
from numpy import linspace, sum, absolute


def fp(v, i):
    p = sum(v.period_magnitude_data * i.period_magnitude_data) / len(v.period_magnitude_data)
    s = v.rms * i.rms
    return p/s


class PlotFactory(object):
    def __init__(self):
        self.show_plot = True
        self.show_legend = True
        self.save_fig = True
        self.xscale = "linear"
        self.yscale = "linear"

    def plot_signals(self, plot_config, all_signals):
        for graph in plot_config:
            title = graph
            properties = plot_config[graph]
            signals = properties["signals"]
            plot_type = properties["plot_type"]
            xlabel = properties["xlabel"]
            ylabel = properties["ylabel"]
            xscale = properties["xscale"]
            yscale = properties["yscale"]
            display_values = properties["display_values"]
            signals_list = [all_signals[name] for name in all_signals if name in signals]
            signal_label_suffixes = {}
            for dsignal in signals_list:
                suffix = ""
                for value in display_values:
                    suffix += " " + value.upper() + "=" + "%.2f" % eval("dsignal"+"."+value)
                signal_label_suffixes[dsignal.name] = suffix
            if plot_type == "linear":
                self.linear_plot(title, xlabel, ylabel, xscale, yscale, signal_label_suffixes, signals_list)
            elif plot_type == "harmonic":
                self.linear_plot(title, xlabel, ylabel, xscale, yscale, signal_label_suffixes, signals_list)
            else:
                return

    def linear_plot(self, title, xlabel, ylabel, xscale, yscale, signal_label_suffixes, discrete_signals):
        if xscale is None:
            xscale = self.xscale
        if yscale is None:
            yscale = self.yscale

        figure = plt.figure()
        subplot = figure.add_subplot(111)
        subplot.set_title(title)
        subplot.set_xlabel(xlabel)
        subplot.set_ylabel(ylabel)
        subplot.set_xscale(xscale)
        subplot.set_yscale(yscale)
        
        color = iter(cm.rainbow(linspace(0, 1, len(discrete_signals))))
        for ds in discrete_signals:
            subplot.plot(ds.time_data, ds.magnitude_data, color=next(color), label=ds.name+signal_label_suffixes[ds.name])
        
        if self.show_legend:
            subplot.legend()
        if self.show_plot:
            plt.show()
        if self.save_fig:
            figure.savefig("img/"+title+".png")
    
    def harmonic_plot(self, title, xlabel, ylabel, xscale, yscale, signal_label_suffixes, discrete_signals):
        if xscale is None:
            xscale = self.xscale
        if yscale is None:
            yscale = self.yscale

        figure = plt.figure()
        subplot = figure.add_subplot(111)
        subplot.set_title(title)
        subplot.set_xlabel(xlabel)
        subplot.set_ylabel(ylabel)
        subplot.set_xscale(xscale)
        subplot.set_yscale(yscale)
        
        for ds in discrete_signals:
            N = len(ds.magnitude_data)
            T = ds.period
            y = ds.magnitude_data
            yf = fft(y)
            xf = fftfreq(N, T)
            xfs = fftshift(xf)
            yplot = fftshift(yf)
            subplot.plot(xfs+ds.freq, 1.0/N * absolute(yplot), label=ds.name+signal_label_suffixes[ds.name])
        
        if self.show_legend:
            subplot.legend()
        if self.show_plot:
            plt.show()
        if self.save_fig:
            figure.savefig("img/"+title+".png")

    def set_show_plot(self, boolean):
        self.show_plot = boolean

    def set_show_legend(self, boolean):
        self.show_legend = boolean

    def set_save_figure(self, boolean):
        self.save_fig = boolean

    def set_xscale(self, value):
        self.xscale = value

    def set_yscale(self, value):
        self.yscale = value
