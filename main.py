from signal_factory import SignalFactory
from plot_factory import PlotFactory

DEFAULT_PSIM_SKIP_HEADER = 1
DEFAULT_PSIM_SKIP_FOOTER = 833
DEFAULT_PSIM_TIME_SHIFTING = -0.1
DEFAULT_PSIM_OFFSET = 0
DEFAULT_PSIM_FREQ = 60
DEFAULT_OSC_SKIP_HEADER = 367
DEFAULT_OSC_SKIP_FOOTER = 49
DEFAULT_OSC_TIME_SHIFTING = 0.01766
DEFAULT_OSC_OFFSET = 0
DEFAULT_OSC_FREQ = 60

SIGNAL_CONFIG = {"C1 Vin Psim": {"path": "data/psim/circ1.csv",
                                 "data_type": "psim",
                                 "skip_header": None,
                                 "skip_footer": None,
                                 "offset": None,
                                 "time_shifting": None,
                                 "freq": None,
                                 "col": 3},
                 "C1 Vin Osc": {"path": "data/osc/ALL0000/F0000CH1.csv",
                                "data_type": "osc",
                                "skip_header": None,
                                "skip_footer": None,
                                "offset": None,
                                "time_shifting": None,
                                "freq": None,
                                "col": None}
                 }

DEFAULT_SHOW_LEGEND = True
DEFAULT_SHOW_PLOT = True
DEFAULT_SAVE_FIG = True
DEFAULT_XSCALE = "linear"
DEFAULT_YSCALE = "linear"

PLOT_CONFIG = {"Circuit 1 PSIM - Vin": {"signals": ["C1 Vin Psim", "C1 Vin Osc"],
                                        "plot_type": "linear",
                                        "xlabel": "Time",
                                        "ylabel": "Magnitude",
                                        "xscale": None,
                                        "yscale": None,
                                        "display_values": ["rms"]
                                        },
               "Circuit 1 PSIM - Vin PSIM": {"signals": ["C1 Vin Psim"],
                                             "plot_type": "linear",
                                             "xlabel": "Time",
                                             "ylabel": "Magnitude",
                                             "xscale": None,
                                             "yscale": None,
                                             "display_values": ["mean", "thd", "max"]
                                             }
               }

sf = SignalFactory()
sf.set_psim_skip_header(DEFAULT_PSIM_SKIP_HEADER)
sf.set_psim_skip_footer(DEFAULT_PSIM_SKIP_FOOTER)
sf.set_psim_time_shifting(DEFAULT_PSIM_TIME_SHIFTING)
sf.set_psim_offset(DEFAULT_PSIM_OFFSET)
sf.set_psim_freq(DEFAULT_PSIM_FREQ)
sf.set_osc_skip_header(DEFAULT_OSC_SKIP_HEADER)
sf.set_osc_skip_footer(DEFAULT_OSC_SKIP_FOOTER)
sf.set_osc_time_shifting(DEFAULT_OSC_TIME_SHIFTING)
sf.set_osc_offset(DEFAULT_OSC_OFFSET)
sf.set_osc_freq(DEFAULT_OSC_FREQ)
sf.add_signals(SIGNAL_CONFIG)

pf = PlotFactory()
pf.set_show_plot(DEFAULT_SHOW_LEGEND)
pf.set_show_legend(DEFAULT_SHOW_PLOT)
pf.set_save_figure(DEFAULT_SAVE_FIG)
pf.set_xscale(DEFAULT_XSCALE)
pf.set_yscale(DEFAULT_YSCALE)
pf.plot_signals(PLOT_CONFIG, sf.get_all_signals())
