# LAB-POTENCIA

Create graphs for UFSC EEL7074 laboratory reports with PSIM and OSCILOSCOPE signal data.

---

## Features

- Plot any number of signals in one graphic
- Two types of plot: linear (time domain) and harmonic (frequency domain)
- Add signal properties such as RMS, MEAN, MAX, MIN, THD and PF to the graphic
- Set labels for each signal, labels for X and Y axis, graphic title

---

## Dependencies

- Pyhton 2.7
- Matplotlib
- Numpy
- Scipy
- Copy

---

## How to use

1. With PSIM, generate the graphs and export them as *.csv* format to the folder *data/psim*;
2. After exported all data from a digital oscilloscope to a pendrive, move all folders like *ALLXXXX* to *data/osc*;
3. On [*main.py*](https://bitbucket.org/samuelnoll/lab-potencia/src/master/main.py) edit the **dafault parameters**, **signals** you're gonna use and **graphics** you wanna create;
    - **Default parameters**:
        - *SKIP_HEADER* is how many lines you want do skip from file begginig. It's useful to adjust the signal begining;
        - *SKIP_FOOTER* is how many lines you want do skip from file endind. It's useful to adjust the signal ending;
        - *TIME_SHIFTING* is hiw much time you want to add to the signal. It's useful to compensate the phase shift from diferent signals;
        - *OFFSET* is how much you want to add to a signal, changing its mean value;
        - *FREQ* is the frequency of the signal;
        - *SHOW_LEGEND* is a boolean that stands for showing the legend on all graphics;
        - *SHOW_PLOT* is a boolean that stands for showing all graphics;
        - *SAVE_FIG* is a boolean that stands saving all graphics;
        - *XSCALE* is the scale of the X axis: linear or logarithm;
        - *YSCALE* is the scale of the Y axis: linear or logarithm.
    - **Setting signals**: edit *SIGNAL_CONFIG* dictionary. Each *key* is the signal name, and each *value* are reading parameters for the especific corresponding signal. You may put *None* in some parameters if you want them to be the default value. 
        - *path* is the path of the *.csv* file where the signal is;
        - *data_type* is which source created the *.csv*, either "psim" or "osc"; 
        - *skip_header* is how many lines you want do skip from file beginig;
        - *skip_footer* is how many lines you want do skip from file endind;
        - *offset* is how much you want to add to a signal, changing its mean value;
        - *time_shifting* is how much time you want to add to the signal, changing its phase;
        - *freq* is the frequency of the signal;
        - *col* is which column in *.csv* the signal is. Only *psim* signals need this parameter.
    - **Setting graphics**: edit *PLOT_CONFIG* dictionary. Each *key* is the graphic title, and each *value* are parameters for plotting the corresponding graphic. You may put *None* in some parameters if you want them to be the default value.
        - *signals* is a list of signals name that will be shown on graphic;
        - *plot_type* is the plot type, either "linear" or "harmonic";
        - *xlabel* is the label of X axis;
        - *ylabel* is the label of Y axis;
        - *xscale* is the scale of the X axis: linear or logarithm;
        - *yscale* is the scale of the Y axis: linear or logarithm;
        - *display_values* is a list of values you want to show for each signal: *rms*, *mean*, *max*, *min*, *thd* and *pf*.
    
4. Run [*main.py*](https://bitbucket.org/samuelnoll/lab-potencia/src/master/main.py) and get the generated graphics at the folder *img*.