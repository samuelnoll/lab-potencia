from scipy.signal import blackmanharris
from numpy.fft import rfft, irfft
import numpy as np
import copy


class DiscreteSignal(object):
    def __init__(self, discrete_data, name, freq=None):
        self.name = name
        self.time_data = discrete_data[0]
        self.magnitude_data = discrete_data[1]
        self.freq = freq
        self.period = None
        self.sample_rate = None
        self.sample_spacing = None
        self.period_time_data = None
        self.period_magnitude_data = None
        self.rms = None
        self.mean = None
        self.min = None
        self.max = None
        self.thd = None
        self._calculate_properties()

    def __mul__(self, other):
        new_discrete_signal = copy.deepcopy(self)
        new_discrete_signal.magnitude_data = other * new_discrete_signal.magnitude_data
        new_discrete_signal.period_magnitude_data = other * new_discrete_signal.period_magnitude_data
        return new_discrete_signal

    def __rmul__(self, other):
        new_discrete_signal = copy.deepcopy(self)
        new_discrete_signal.magnitude_data = other * new_discrete_signal.magnitude_data
        new_discrete_signal.period_magnitude_data = other * new_discrete_signal.period_magnitude_data
        return new_discrete_signal
    
    def _calculate_properties(self):
        if self.freq is None:
            self.freq = self._calculate_freq()
        else:
            self.freq = float(self.freq)
        
        self.period = 1/self.freq

        self.sample_spacing = abs(self.time_data[1] - self.time_data[0])
        self.sample_rate = 1/self.sample_spacing
        max_period_index = int(self.sample_rate/self.freq)
        self.period_time_data = self.time_data[:max_period_index]
        self.period_magnitude_data = self.magnitude_data[:max_period_index]
    
        self.rms = np.sqrt(np.mean(self.period_magnitude_data**2))
        
        self.mean = np.mean(self.period_magnitude_data)
        
        self.max = np.max(self.magnitude_data)
        
        self.min = np.min(self.magnitude_data)
        
        self.thd = self._calculate_thd()

    def _calculate_freq(self):
        return float(60)
    
    def _calculate_thd(self):
        magnitude_data = self.magnitude_data - np.mean(self.magnitude_data)
        windowed = magnitude_data * blackmanharris(len(magnitude_data))
    
        # Measure the total signal before filtering but after windowing
        total_rms = np.sqrt(np.mean(np.abs(windowed)**2))
    
        # Find the peak of the frequency spectrum (fundamental frequency), and filter 
        # the signal by throwing away values between the nearest local minima
        f = rfft(windowed)
        i = np.argmax(abs(f))
        
        # Find range between nearest local minima from peak at index x
        lowermin = 0
        uppermin = 0
        f_abs = abs(f)
        for j in np.arange(i+1, len(f_abs)):
            if f_abs[j+1] >= f_abs[j]:
                uppermin = i
                break
        for j in np.arange(i-1, 0, -1):
            if f_abs[j] <= f_abs[j-1]:
                lowermin = j + 1
                break
        f[lowermin: uppermin] = 0
    
        # Transform noise back into the signal domain and measure it
        noise = irfft(f)
        thdn = np.sqrt(np.mean(np.abs(noise)**2)) / total_rms
        return thdn
