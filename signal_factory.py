from numpy import genfromtxt, transpose
from discrete_signal import DiscreteSignal


class SignalFactory(object):
    def __init__(self):
        self.discrete_signals = {}
        self.psim_skip_header = 1
        self.psim_skip_footer = 2500
        self.psim_time_shifting = 0
        self.psim_offset = 0
        self.psim_freq = 60
        self.osc_skip_header = 1201
        self.osc_skip_footer = 49
        self.osc_time_shifting = 0
        self.osc_offset = 0
        self.osc_freq = 60

    def add_signals(self, signal_config):
        for dsignal in signal_config:
            name = dsignal
            properties = signal_config[dsignal]
            path = properties["path"]
            data_type = properties["data_type"]
            skip_header = properties["skip_header"]
            skip_footer = properties["skip_footer"]
            time_shifting = properties["time_shifting"]
            offset = properties["offset"]
            freq = properties["freq"]
            col = properties["col"]
            if data_type == "psim":
                self.add_psim_signal(name, path, skip_header=skip_header, skip_footer=skip_footer,
                                     time_shifting=time_shifting, offset=offset, freq=freq, col=col)
            elif data_type == "osc":
                self.add_osc_signal(name, path, skip_header=skip_header, skip_footer=skip_footer,
                                    time_shifting=time_shifting, offset=offset, freq=freq)
            else:
                return

    def add_psim_signal(self, name, path, skip_header=None, skip_footer=None, time_shifting=None, offset=None,
                        freq=None, col=1):
        if skip_header is None:
            skip_header = self.psim_skip_header
        if skip_footer is None:
            skip_footer = self.psim_skip_footer
        if time_shifting is None:
            time_shifting = self.psim_time_shifting
        if offset is None:
            offset = self.psim_offset
        if freq is None:
            freq = self.psim_freq

        signal_data = self._read_psim_signal_data(path, col, skip_header, skip_footer)

        signal_data[0] += time_shifting
        signal_data[1] += offset

        self.discrete_signals[name] = DiscreteSignal(signal_data, name, freq=freq)

    def add_osc_signal(self, name, path, skip_header=None, skip_footer=None, time_shifting=None,
                       offset=None, freq=None):
        if skip_header is None:
            skip_header = self.osc_skip_header
        if skip_footer is None:
            skip_footer = self.osc_skip_footer
        if time_shifting is None:
            time_shifting = self.osc_time_shifting
        if offset is None:
            offset = self.osc_offset
        if freq is None:
            freq = self.osc_freq

        signal_data = self._read_osc_signal_data(path, skip_header, skip_footer)

        signal_data[0] += time_shifting
        signal_data[1] += offset

        self.discrete_signals[name] = DiscreteSignal(signal_data, name, freq=freq)

    @staticmethod
    def _read_psim_signal_data(path, col, skip_header, skip_footer):
        psim_data = transpose(genfromtxt(path, delimiter=',', skip_header=skip_header, skip_footer=skip_footer))
        return [psim_data[0], psim_data[col]]

    @staticmethod
    def _read_osc_signal_data(path, skip_header, skip_footer):
        osc_data = genfromtxt(path, delimiter=',', skip_header=skip_header, skip_footer=skip_footer,
                              names=['a', 'b', 'c', 'x', 'y', 'd'])
        return [osc_data["x"], osc_data["y"]]

    def get_all_signals(self):
        return self.discrete_signals

    def get_all_signals_names(self):
        return self.discrete_signals.keys()

    def get_signal(self, name):
        return self.discrete_signals[name]

    def set_psim_skip_header(self, value):
        self.psim_skip_header = value

    def set_psim_skip_footer(self, value):
        self.psim_skip_footer = value

    def set_psim_time_shifting(self, value):
        self.psim_time_shifting = value

    def set_psim_offset(self, value):
        self.psim_offset = value

    def set_psim_freq(self, value):
        self.psim_freq = value

    def set_osc_skip_header(self, value):
        self.osc_skip_header = value

    def set_osc_skip_footer(self, value):
        self.osc_skip_footer = value

    def set_osc_time_shifting(self, value):
        self.osc_time_shifting = value

    def set_osc_offset(self, value):
        self.osc_offset = value

    def set_osc_freq(self, value):
        self.osc_freq = value
